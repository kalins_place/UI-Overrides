# Windows (10) Interface Customization

Windows can be altered in a variety of ways beyond its exposed settings by using registry tweaks, security controls, group policies, and third-party tools.

<br><br><br><br>

## Taskbar and Start menu
Set Windows to light theme and taskbar to small buttons. Then with [Open-Shell](https://github.com/Open-Shell/Open-Shell-Menu/) settings, load the custom [Start button](openshell-small-start.png) and tiled [taskbar background](openshell-small-background.png) with opaque look.
| state | screenshot |
| :--- | :--- |
| idle | ![Open-Shell](screenshot-openshell-small-start.png) |
| hover | ![Open-Shell](screenshot-openshell-small-start-hover.png) |
| active | ![Open-Shell](screenshot-openshell-small-start-active.png) |

<br><br><br><br>

## Explorer
// TODO: removing default context menus, automating default settings, etc

<br><br><br><br>

### Context Menu: Open Command Prompt in the selected or current folder
I often need to open `cmd` in specific folders (and no I did not ask for PowerShell). The `Extended` line makes it so that the menu item only appears when holding the `Shift` key, while `background` is for right-clicking an empty space in the current folder.

![Command Prompt in context menu](screenshot-explorer-context-cmd.png)

```
Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\Directory\shell\kp-commandprompt]
"Extended"=""
@="Open Command Prompt"
"Icon"="\"%SystemRoot%\\System32\\cmd.exe\""

[HKEY_CLASSES_ROOT\Directory\shell\kp-commandprompt\Command]
@="cmd.exe /k cd %1"

[HKEY_CLASSES_ROOT\Directory\background\shell\kp-commandprompt]
@="Open Command Prompt"
"Icon"="\"%SystemRoot%\\System32\\cmd.exe\""

[HKEY_CLASSES_ROOT\Directory\background\shell\kp-commandprompt\Command]
@="cmd.exe /s /k pushd \"%V\""
```

<br><br><br><br>

### Context Menu: Execute various batch conversion scripts in the current folder
Creating submenus is similar to the above. For example, I like the file size reduction when compressing with JPEG-XL so I made it easy to batch convert all images in the current folder by executing some helper scripts. For the moment, there are some artifacts with game UI screenshots in JXL versions beyond 0.70 and I only use the newer version for lossless conversions. The same technique can be expanded to videos by calling other scripts or even ffmpeg directly. Lastly, the `touch` command retains the original file modified timestamps. *However, `cjxl` fails to access unicode filenames because `cmd` does not support unicode when using this method.*

![Command Prompt in context menu](screenshot-explorer-context-batch.png)

```
Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\Directory\Background\shell\kp-batchconverters]
"MUIVerb"="Batch Convert"
"SubCommands"=""
"Icon"="%SystemRoot%\\System32\\shell32.dll,326"

[HKEY_CLASSES_ROOT\Directory\Background\shell\kp-batchconverters\shell]

[HKEY_CLASSES_ROOT\Directory\Background\shell\kp-batchconverters\shell\jxl100]
@="All PNGs to lossless JXL 10.2"
"Icon"="C:\\Program Files\\IrfanView\\Plugins\\Icons.dll,21"

[HKEY_CLASSES_ROOT\Directory\Background\shell\kp-batchconverters\shell\jxl100\command]
@="cmd.exe /s /k pushd \"%V\" && \"C:\\Program Files\\COOLSCRIPTS\\kp-jxl-q100.bat\" && exit"

[HKEY_CLASSES_ROOT\Directory\Background\shell\kp-batchconverters\shell\jxl95]
@="All PNGs to lossy JXL 0.7 q95"
"Icon"="C:\\Program Files\\IrfanView\\Plugins\\Icons.dll,21"

[HKEY_CLASSES_ROOT\Directory\Background\shell\kp-batchconverters\shell\jxl95\command]
@="cmd.exe /s /k pushd \"%V\" && \"C:\\Program Files\\COOLSCRIPTS\\kp-jxl-q95.bat\" && exit"

[HKEY_CLASSES_ROOT\Directory\Background\shell\kp-batchconverters\shell\jxl98]
@="All PNGs to lossy JXL 0.7 q98"
"Icon"="C:\\Program Files\\IrfanView\\Plugins\\Icons.dll,21"

[HKEY_CLASSES_ROOT\Directory\Background\shell\kp-batchconverters\shell\jxl98\command]
@="cmd.exe /s /k pushd \"%V\" && \"C:\\Program Files\\COOLSCRIPTS\\kp-jxl-q98.bat\" && exit"

[HKEY_CLASSES_ROOT\Directory\Background\shell\kp-batchconverters\shell\jxljpg]
@="All JPGs to transcoded JXL 10.2"
"Icon"="C:\\Program Files\\IrfanView\\Plugins\\Icons.dll,14"

[HKEY_CLASSES_ROOT\Directory\Background\shell\kp-batchconverters\shell\jxljpg\command]
@="cmd.exe /s /k pushd \"%V\" && \"C:\\Program Files\\COOLSCRIPTS\\kp-jxl-jpg.bat\" && exit"
```

#### kp-jxl-q100.bat
```
@echo OFF

TITLE jxl10-LL %CD%

echo:
echo "Converting all PNGs to lossless JXL 10.2"
echo:
pause

for %%A IN ("*.png") DO echo: && echo "%%~NA.png" && "C:\Program Files\JPEG-XL-10.2\cjxl.exe" ".\%%~NA.png" ".\%%~NA-ll.jxl" --verbose --quality=100 && touch ".\%%~NA-ll.jxl" -r ".\%%~NA.png"

echo:
pause
```

#### kp-jxl-q95.bat
```
@echo OFF

TITLE jxl07-q95 %CD%

echo:
echo "Converting all PNGs to lossy JXL 0.7 q95"
echo:
pause

for %%A IN ("*.png") DO echo: && echo "%%~NA.png" && "C:\Program Files\JPEG-XL-0.70\cjxl.exe" ".\%%~NA.png" ".\%%~NA-q95.jxl" --verbose --resampling=1 --quality=95 && touch ".\%%~NA-q95.jxl" -r ".\%%~NA.png"

echo:
pause
```

#### kp-jxl-q98.bat
```
@echo OFF

TITLE jxl07-q98 %CD%

echo:
echo "Converting all PNGs to lossy JXL 0.7 q98"
echo:
pause

for %%A IN ("*.png") DO echo: && echo "%%~NA.png" && "C:\Program Files\JPEG-XL-0.70\cjxl.exe" ".\%%~NA.png" ".\%%~NA-q98.jxl" --verbose --resampling=1 --quality=98 && touch ".\%%~NA-q98.jxl" -r ".\%%~NA.png"

echo:
pause
```

#### kp-jxl-jpg.bat
```
@echo OFF

TITLE jxl-jpg %CD%

echo:
echo "Transcoding all JPGs to JXL 10.2"
echo:
pause

for %%A IN ("*.jpg") DO echo: && echo "%%~NA.jpg" && "C:\Program Files\JPEG-XL-10.2\cjxl.exe" ".\%%~NA.jpg" ".\%%~NA-tc.jxl" --verbose --lossless_jpeg=1 && touch ".\%%~NA-tc.jxl" -r ".\%%~NA.jpg"

echo:
pause
```

<br><br><br><br>

### Context Menu: Generate a text listing all files on a drive
In Command Prompt, `dir` simply displays a list of files. Let's take it a step further and make a context menu for storage drives that will quickly save a text file on the Desktop that contains a list of everything on the selected drive.

![Drive file listing context menu](screenshot-explorer-context-drivelisting1.png)
![Drive file listing context menu](screenshot-explorer-context-drivelisting2.png)

```
Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\Drive\shell\kp-drivelisting]
@="Make a list of all files"
"Icon"="%SystemRoot%\\System32\\shell32.dll,321"

[HKEY_CLASSES_ROOT\Drive\shell\kp-drivelisting\command]
@="\"C:\\Program Files\\COOLSCRIPTS\\kp-drivelisting.bat\" \"%1\""
```

#### kp-drivelisting.bat
```
@echo OFF

TITLE File listing for %1
cd /d %1

@For /F "tokens=1,2,3,4 delims=/ " %%A in ('Date /t') do @(
Set DayW=%%A
Set Day=%%B
Set Month=%%C
Set Year=%%D
Set DateString=%%D-%%B-%%C
)

dir %1 /a /og /on /s > "%USERPROFILE%\Desktop\filelisting - %cd:~0,1% (%DateString%).txt"
```