# Firefox Interface Customization
Available for many years, but not enabled by default, Firefox has mechanisms for visually altering its interface using simple CSS and even altering some of its interactive functionality using JavaScript. The following only focuses on changing how the browser looks, but not changing how it behaves. It's helpful to have a basic understanding of how (HTML) web pages are styled (with CSS).

### Process: *Inspect the browser with its own web developer tools*
1. Open Firefox's Web Developer Tools (`Ctrl+Shift+I` / `F12` / right-click `Inspect` on any page).
2. Go to its settings (click the three dots on the top right of the developer tools panel or window).
3. Make sure `Enable browser chrome ... debugging toolboxes` and `Enable remote debugging` are enabled at the bottom of the settings screen.
4. You can close the current developer tools and now press `Ctrl+Alt+Shift+I`, allowing the connection request that pops up.
5. Now use this new Browser Toolbox window to start poking at the browser's interface by going through various HTML elements and CSS rules. Find out how different elements are named and what styles they use. You could also rapidly test out changes without restarting the browser.

### Prerequisite: *Enable User Stylesheets*
1. In [about:config](https://support.mozilla.org/en-US/kb/about-config-editor-firefox), change `toolkit.legacyUserProfileCustomizations.stylesheets` to `true`.
2. In your [profile folder](https://support.mozilla.org/en-US/kb/profiles-where-firefox-stores-user-data), create a folder named `chrome`, and in the subfolder create a plain text file named `userChrome.css`.
3. Paste the following CSS snippets into that file as you wish, although I believe red close buttons and brighter-than-background input fields are required accessibility principles. The snippets mostly independent from each other, except for the use of custom named colors, so you may want to always include them or manually adjust other snippets.

<br><br><br><br>

## Define custom named colors for later use
```CSS
:root {
	--kp-red-win10: #e81123;
	--kp-blue-win10: #0078d7;
	--kp-gray-win10context: #eee;
	--kp-gray-win10buttonactive: #ccc;
	--kp-blue-ff: #0074e8;
}
```

<br><br><br><br>

## Override toolbar colors to be cleaner, toolbar button hovers to be more consistent, input box outlines, and use Windows 10 based colors for maximize and close buttons on hover
| | |
| ---: | :--- |
| **BEFORE** | ![Toolbar Before](firefox-toolbar-before.png) |
| **AFTER** | ![Toolbar After](firefox-toolbar-after.png) |

| BEFORE | AFTER |
| ---: | :--- |
| ![Bookmarks Before](firefox-bookmarks-before.png) | ![Bookmarks Before](firefox-bookmarks-after.png) |
| ![Maximize Before](firefox-maximize-before.png) | ![Maximize After](firefox-maximize-after.png) |
```CSS
/* //REQUIRES: default Firefox "System" theme, tested running on Windows 10 with its light theme */
/* the negations avoid affecting private windows and chosen browser themes */
:root:not([lwtheme]) {
	--toolbar-bgcolor: #fcfcfc !important;
	--toolbar-field-border-color: #d9d9d9 !important;
	--toolbar-field-background-color: #ffffff !important;
	--urlbar-box-bgcolor: #f5f5f5 !important;
	--button-hover-bgcolor: rgba(0,0,0,0.075) !important;
	--chrome-content-separator-color: #e8e8e8 !important;
}
:root:not([lwtheme]) #navigator-toolbox {
	background-image: linear-gradient(var(--kp-gray-win10context),var(--chrome-content-separator-color));
}
/* //TODO: also apply input box background, border, and active outline to non-main windows */
:root:not([lwtheme]) #editBookmarkPanel {
	--panel-background: var(--toolbar-bgcolor) !important;
}
:root:not([lwtheme]) #editBMPanel_namePicker,
:root:not([lwtheme]) #editBMPanel_tagsField,
:root:not([lwtheme]) #editBMPanel_folderTree,
:root:not([lwtheme]) #editBMPanel_tagsSelector {
	border: 1px solid var(--toolbar-field-border-color) !important;
}
:root:not([lwtheme]) #urlbar[focused] > #urlbar-background,
:root:not([lwtheme]) #searchbar:focus-within,
:root:not([lwtheme]) .findbar-textbox:focus {
	box-shadow: none !important;
	outline-color: var(--kp-blue-ff) !important;
}
:root:not([lwtheme]):-moz-window-inactive {
	--toolbar-field-border-color: var(--urlbar-box-bgcolor) !important;
}
.titlebar-close:hover {
	background-color: var(--kp-red-win10) !important;
}
.titlebar-max:hover,
.titlebar-restore:hover {
	background-color: var(--kp-blue-win10) !important;
	color: #fff !important;
}
.titlebar-min:hover,
.tabbrowser-tab:hover .tab-background:not([selected]) {
	background-color: var(--button-hover-bgcolor) !important;
}
.titlebar-button:active {
	background-color: var(--kp-gray-win10buttonactive) !important;
	color: #fff !important;
}
```

<br><br><br><br>

## Change tab close buttons into red circles
| BEFORE | AFTER |
| ---: | :--- |
| ![Close Tab Before](firefox-closetab1-before.png) | ![Close Tab After](firefox-closetab1-after.png) |
| ![Close Tab Before](firefox-closetab2-before.png) | ![Close Tab After](firefox-closetab2-after.png) |
```CSS
.tab-close-button {
	width: 20px !important;
	height: 20px !important;
	padding: 4px !important;
	border-radius: 50% !important;
}
.all-tabs-close-button {
	padding: 0 !important;
}
.all-tabs-close-button > .toolbarbutton-icon {
	padding: 4px !important;
	border-radius: 50% !important;
}
.all-tabs-close-button:hover {
	background-color: unset !important;
}
.tab-close-button.close-icon:hover,
.all-tabs-close-button:hover > .toolbarbutton-icon {
	background-color: var(--kp-red-win10) !important;
	color: #fff !important;
}
.tab-close-button.close-icon:active,
.all-tabs-close-button:active > .toolbarbutton-icon {
	background-color: var(--kp-gray-win10buttonactive) !important;
}
```

<br><br><br><br>

## Re-attach tabs and tweak style, slightly thin tabs and adddressbar
| | |
| ---: | :--- |
| **BEFORE** | ![Tabs After](firefox-tabs-before.png) |
| **AFTER** | ![Tabs After](firefox-tabs-after.png) |
```CSS
.tab-background {
	/*--tab-border-radius: 2px !important; /* optionally less rounding */
	border-radius: var(--tab-border-radius) var(--tab-border-radius) 0 0 !important;
	margin-top: 3px !important;
	margin-bottom: 0 !important;
}
:root:not([lwtheme]) .tab-background:is([selected],[multiselected]) {
	background-image: linear-gradient(#fff,var(--toolbar-bgcolor)); /* yet this does not affect private windows */
	box-shadow: 0 0 4px rgba(0,0,0,0.2) !important;
}
:root:not([lwtheme]) .tab-background:is([selected],[multiselected]):-moz-window-inactive {
	background-image: none !important;
	background-color: var(--toolbar-bgcolor) !important;
	box-shadow: none !important;
}
#TabsToolbar {
	margin-bottom: 0px !important;
}
:root {
	--tab-min-height: 30px !important;
}
#urlbar,
#searchbar {
	--urlbar-min-height: 30px !important;
}
```

<br><br><br><br>

## Add tab dividers
| | |
| ---: | :--- |
| **BEFORE** | ![Tab Dividers Before](firefox-tabdividers-before.png) |
| **AFTER** | ![Tab Dividers After](firefox-tabdividers-after.png) |
| **AFTER (Private)** | ![Tab Dividers After](firefox-tabdividers-private-after.png) |
```CSS
.tabbrowser-tab {
	position: relative;
}
/* //TODO: minor defect, first divider to left of active tab needs to hide */
.tabbrowser-tab:not([selected]):after {
	content: "";
	display: block;
	border-left: 1px solid var(--button-hover-bgcolor);
	height: 66%;
	position: absolute;
	top: 50%;
	right: 0;
	transform: translateY(-50%);
}
```

<br><br><br><br>

## Expand folder tree in edit bookmark popup
| BEFORE | AFTER |
| ---: | :--- |
| ![Bookmarks Tree Before](firefox-bookmarkstree-before.png) | ![Bookmarks Tree Before](firefox-bookmarkstree-after.png) |
```CSS
#editBMPanel_folderTree {
	/*min-width: 350px !important; /* optionally control the width */
	min-height: 225px !important;
}
```

<br><br><br><br>

## Windows 10 taskbar styled context menus with minimum width
| BEFORE | AFTER *(non-main windows currently lack color)* |
| ---: | :--- |
| ![Context Style Before](firefox-contextstyle-before.png) | ![Context Style After](firefox-contextstyle-after.png) |
| ![Context Style Before](firefox-contextwidth-before.png) | ![Context Style After](firefox-contextwidth-after.png) |
```CSS
/* //TODO: also colorize context menus in non-main windows */
:root:not([lwtheme]) #mainPopupSet menupopup,
:root:not([lwtheme]) #navigator-toolbox menupopup {
	--panel-background: var(--kp-gray-win10context) !important;
}
:root:not([lwtheme]) #mainPopupSet menu:where([_moz-menuactive="true"]:not([disabled="true"])),
:root:not([lwtheme]) #mainPopupSet menuitem:where([_moz-menuactive="true"]:not([disabled="true"])),
:root:not([lwtheme]) #mainPopupSet .menuitem-iconic[_moz-menuactive="true"] .menu-iconic-icon,
:root:not([lwtheme]) #navigator-toolbox menu:where([_moz-menuactive="true"]:not([disabled="true"])),
:root:not([lwtheme]) #navigator-toolbox menuitem:where([_moz-menuactive="true"]:not([disabled="true"])) {
	background-color: #fff !important;
}
menupopup {
	--panel-padding: var(--panel-padding-block) 0 !important;
	--panel-border-radius: 0 !important;
	--panel-shadow: 4px 4px 3px -3px rgba(0,0,0,0.75) !important;
	min-width: 125px !important;
}
```

<br><br><br><br>

## Remove context menu items
| BEFORE | AFTER |
| ---: | :--- |
| ![Context Menu Before](firefox-contexthide1-before.png) | ![Context Menu After](firefox-contexthide1-after.png) |
| ![Context Menu Before](firefox-contexthide2-before.png) | ![Context Menu After](firefox-contexthide2-after.png) |
| ![Context Menu Before](firefox-contexthide3-before.png) | ![Context Menu After](firefox-contexthide3-after.png) |
```CSS
#context-sendimage,
#context-sep-setbackground,
#context-setDesktopBackground,
#context-sendimage,
#context-sendvideo,
#context-sendaudio,
#context-print-selection,
#context-stripOnShareLink,
#strip-on-share,
.share-tab-url-item {
	display: none !important;
}
```

<br><br><br><br>

## Speed up fullscreen transition animations
```CSS
/* animate how quickly the toolbar slides up during F11 */
#navigator-toolbox[fullscreenShouldAnimate] {
	transition: 0.25s margin-top ease-out !important;
}
```
| about:config Pref | Value | Usage |
| :--- | :--- | :--- |
| full-screen-api.warning.timeout | 1234 | *affects how long the "[site] is now fullscreen" alert is shown* |
| full-screen-api.transition-duration.enter | 50 50 | *animated blank screen fades in milliseconds* |
| full-screen-api.transition-duration.leave | 50 50 | *animated blank screen fades in milliseconds* |
| full-screen-api.transition.timeout | 200 | *milliseconds until auto-exiting fullscreen (during an error or from ctrl+w)* |

<br><br><br><br>
