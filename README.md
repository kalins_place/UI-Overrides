# User Interface Overrides

A collection alterations to software interfaces.

## [Firefox](Firefox/README.md)
Firefox has decent mechanisms to alter its interface. I have re-attached tabs, replaced button icons, tweaked sizing, and more.

## [Windows](Windows/README.md)
There is a lot more that can be changed beyond what's available as user-facing settings. I have created handy new context menus, decluttered, and more.

## Linux
// TODO: Adwaita/XFCE GTK CSS

## [Web Pages](Web.md)
Fix, tweak, or re-design various websites.