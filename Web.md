# Web Page Overrides

Some browsers or browser addons contain features that can alter web page content. Certain common adblockers enable basic CSS manipulation so a dedicated styling addon is often not necessary. The following is uBlock Origin syntax or equivalent.

```

! remove sign-in with widgets (need to temporarily comment if using the services)
||accounts.google.com
||google.com/*$subdocument
||facebook.com/*$subdocument
||twitter.com/*$subdocument
||linkedin.com/*$subdocument

! unsticky header when scrolling and/or full viewport scrollbar (enables full-page screenshots)
www.fab.com##.fabkit-MegaMenu-root:style(position:absolute !important)
soundcloud.com##.l-fixed-top-one-column:style(padding-top:0 !important)
soundcloud.com##.l-top:style(position:unset !important)
bugzilla.mozilla.org###wrapper:style(position:unset !important; display:block !important; overflow:unset !important; width:unset !important; height:unset !important)
bugzilla.mozilla.org###header:style(flex:unset !important;)
bugzilla.mozilla.org##bugzilla-body:style(flex:unset !important; display: block !important; overflow:unset !important)
www.kaggle.com##html:style(overflow:unset !important)
www.kaggle.com##body:style(overflow:unset !important)
www.kaggle.com###site-content:style(overflow:unset !important; height: unset !important)

! hide cookie notices
##.mw-cookiewarning-container
##.js-consent-banner
###gdpr-modal-bottom

! remove obnoxious on-screen and off-screen boxes and borders that ruin full-page screenshots
news.ubisoft.com##.transition-wrapper
winaero.com##body::before
winaero.com##body::after
winaero.com##body > #page:style(margin:0 !important)

! remove buttons to view images fullscreen without obstructions, was used on marketplace before fab, might not be useful anymore
!www.unrealengine.com##.image-gallery-image::before
!www.unrealengine.com##.image-gallery-left-nav
!www.unrealengine.com##.image-gallery-right-nav

! fix image popups appearing behind the article content
fortnite.fandom.com##.__ns__pop2top:style(z-index:unset !important)

! show all historical occurences instead of a short box with scrollbar
fnbr.co##.item-page-container .occurrences-table-container:style(max-height: unset !important;)

! disable animations on beatport because their broken javascript increases cpu load with every played track until the browser becomes unusable, *side effect: no more waveform previews or current position bar*
www.beatport.com##+js(norafif,u)

```